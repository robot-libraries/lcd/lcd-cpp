# Lcd++

C++ library for Sparkfun serial, I2C and SPI controlled displays using the HD44780. Target is the Raspberry Pi us PiGPIO library.

### References
   Sparkfun: https://learn.sparkfun.com/tutorials/avr-based-serial-enabled-lcds-hookup-guide/introduction
   
   PiGpio: https://abyz.me.uk/rpi/pigpio/index.html

