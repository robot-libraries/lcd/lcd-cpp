//======================================================================================================================
// 2022 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//		 File: LcdSerial.cpp
//
//     Author: rmerriam
//
//    Created: Jan 4, 2022
//
//======================================================================================================================
#include <cstring>
#include <iostream>

#include <LcdSerial.h>
//----------------------------------------------------------------------------------------------------------------------
namespace lcd {
    //----------------------------------------------------------------------------------------------------------------------
    LcdSerial::LcdSerial(PiGpio const& pigpio, char const* ser_tty, unsigned const baud) :
        LcdStream { pigpio } {
        std::cout << "\nserial " << (int)mHandle << '\n';
        mHandle = serial_open(mPiGpio.pi(), const_cast<char*>(ser_tty), baud, 0);
        std::cout << "\nserial " << (int)mHandle << '\n';
        mIsOpen = mHandle >= 0;
    }
    //----------------------------------------------------------------------------------------------------------------------
    LcdSerial::~LcdSerial() {
        serial_close(mPiGpio.pi(), mHandle);
    }
    //======================================================================================================================
    int16_t LcdSerial::write(std::uint8_t const data) const {
        return serial_write_byte(mPiGpio.pi(), mHandle, data);
    }
    //----------------------------------------------------------------------------------------------------------------------
    int16_t LcdSerial::write(char const* const data) const {
        return serial_write(mPiGpio.pi(), mHandle, const_cast<char*>(data), std::strlen(data));
    }
    //----------------------------------------------------------------------------------------------------------------------
    int16_t LcdSerial::write(std::string const& data) const {
        return serial_write(mPiGpio.pi(), mHandle, const_cast<char*>(data.c_str()), data.size());
    }
    //----------------------------------------------------------------------------------------------------------------------
    void LcdSerial::write(lcd_chars const data[], uint16_t const count) const {
        serial_write(mPiGpio.pi(), mHandle, reinterpret_cast<char*>(const_cast<lcd_chars*>(data)), count);
    }

} /* namespace lcd */
