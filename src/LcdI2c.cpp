//======================================================================================================================
// 2022 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//		 File: LcdI2c.cpp
//
//     Author: rmerriam
//
//    Created: Jan 1, 2022
//
//======================================================================================================================
#include <cstring>

#include <LcdI2c.h>
//----------------------------------------------------------------------------------------------------------------------
namespace lcd {

    //----------------------------------------------------------------------------------------------------------------------
    LcdI2c::LcdI2c(PiGpio const& pigpio, uint8_t addr, uint8_t dev) :
        LcdStream { pigpio } {
        mHandle = i2c_open(mPiGpio.pi(), dev, addr, 0);
        mIsOpen = mHandle >= 0;
    }
    //----------------------------------------------------------------------------------------------------------------------
    LcdI2c::~LcdI2c() {
        i2c_close(mPiGpio.pi(), mHandle);
    }
    //======================================================================================================================
    int16_t LcdI2c::write(uint8_t const data) const {
        return i2c_write_byte(mPiGpio.pi(), mHandle, data);
    }
    //----------------------------------------------------------------------------------------------------------------------
    int16_t LcdI2c::write(char const* const data) const {
        return i2c_write_device(mPiGpio.pi(), mHandle, const_cast<char*>(data), std::strlen(data));
    }
    //----------------------------------------------------------------------------------------------------------------------
    int16_t LcdI2c::write(std::string const& data) const {
        return i2c_write_device(mPiGpio.pi(), mHandle, const_cast<char*>(data.c_str()), data.size());
    }
    //----------------------------------------------------------------------------------------------------------------------
    inline void LcdI2c::write(lcd_chars const data[], uint16_t const count) const {
        i2c_write_device(mPiGpio.pi(), mHandle, reinterpret_cast<char*>(const_cast<lcd_chars*>(data)), count);
    }

} /* namespace lcd */
