//======================================================================================================================
// 2022 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http:                                        //www.gnu.org/licenses/>.
//======================================================================================================================
//
//		 File: LcdStream.cpp
//
//     Author: rmerriam
//
//    Created: Jan 1, 2022
//
//======================================================================================================================
#include <iostream>
#include <iomanip>
#include <LcdStream.h>
#include <cstring>
//----------------------------------------------------------------------------------------------------------------------
namespace lcd {
    //----------------------------------------------------------------------------------------------------------------------
    enum struct lcd_chars : uint8_t {
        settings = 0x7C,            //
        reset = 0x08,               //
        contrast = 0x18,            //
        color = 0x2B,               //
        firmware = 0x2C,            //
        clear_cmd = 0x2D,            //
        enable_system_message_display = 0x2e, disable_system_message_display = 0x2f,            //
        backlight_base = 0x80, green_base = 0x9E, blue_base = 0xBC,            //
        //
        //
        command = 0xFE,             //
        return_home = 0x02,            //
        blink_on = 0x01,            //
        cursor_on = 0x02,            //
        display_on = 0x04,            //
        set_cursor_pos = 0x80,            //
        //
        entry_mode_set = 0x04, entryshift_left = 0x02, entryshift_increment = 0x01,            //
        //
        cursor_shift = 0x10,            //
        display_move = 0x08,            //
        cursor_move = 0,                //
        move_right = 0x04, move_left = 0x00,                    //
    };

    //----------------------------------------------------------------------------------------------------------------------
    using enum lcd_chars;
    //======================================================================================================================
    //  UTILITY ROUTINES
    int16_t map(int16_t x, int16_t in_min, int16_t in_max, int16_t out_min, int16_t out_max) {
        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }
    //----------------------------------------------------------------------------------------------------------------------
    template <typename L, typename M>
    inline lcd_chars operator|(L const lhs, M const rhs) {
        return lcd_chars(uint8_t(lhs) | uint8_t(rhs));
    }
    //----------------------------------------------------------------------------------------------------------------------
    template <typename L, typename M>
    inline lcd_chars operator&(L const lhs, M const rhs) {
        return lcd_chars(uint8_t(lhs) & uint8_t(rhs));
    }
    //----------------------------------------------------------------------------------------------------------------------
    template <typename L>
    inline lcd_chars operator~(L const lhs) {
        return lcd_chars( ~uint8_t(lhs));
    }
    //======================================================================================================================
    LcdStream::LcdStream(PiGpio const& pigpio) :
        mPiGpio { pigpio }, mDisplayState { display_move | display_on },                    //
            mDisplayActions { entry_mode_set } {
    }
    //----------------------------------------------------------------------------------------------------------------------
    LcdStream::~LcdStream() {
    }
    //======================================================================================================================
    // COMMANDS
    //----------------------------------------------------------------------------------------------------------------------
    void LcdStream::cursor(bool const on) const {
        displayChange(on, cursor_on);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void LcdStream::blink(bool const on) const {
        displayChange(on, blink_on);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void LcdStream::display(bool const on) const {
        displayChange(on, display_on);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void LcdStream::home() const {
        emit(command, return_home);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void LcdStream::scroll(bool const inc_on) const {
        mDisplayActions = mDisplayActions | entryshift_increment;
        if ( !inc_on) {
            mDisplayActions = mDisplayActions & ~entryshift_increment;
        }
        emit(command, mDisplayActions);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void LcdStream::textLtoR(bool const inc_on) const {
        mDisplayActions = mDisplayActions | entryshift_left;
        if ( !inc_on) {
            mDisplayActions = mDisplayActions & ~entryshift_left;
        }
        emit(command, mDisplayActions);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void LcdStream::move(uint8_t col, uint8_t row) const {
        constinit
        static int const row_offsets[] = { 0x00, 0x40, 0x14, 0x54 };
        emit(command, (set_cursor_pos | (col + row_offsets[row])));
    }
    //----------------------------------------------------------------------------------------------------------------------
    void LcdStream::left(uint8_t cnt) const {
        for (; cnt > 0; --cnt) {
            emit(command, (cursor_shift | cursor_move | move_left));
        }
    }
    //----------------------------------------------------------------------------------------------------------------------
    void LcdStream::right(uint8_t cnt) const {
        for (; cnt > 0; --cnt) {
            emit(command, (cursor_shift | cursor_move | move_right));
        }
    }
    //----------------------------------------------------------------------------------------------------------------------
    void LcdStream::displayLeft(uint8_t cnt) const {
        for (; cnt > 0; --cnt) {
            emit(command, (cursor_shift | display_move | move_left));
        }
    }
    //----------------------------------------------------------------------------------------------------------------------
    void LcdStream::displayRight(uint8_t cnt) const {
        for (; cnt > 0; --cnt) {
            emit(command, (cursor_shift | display_move | move_right));
        }
    }
    //======================================================================================================================
    // SETTINGS
    void LcdStream::clear() const {
        emit(settings, clear_cmd);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void LcdStream::firmware() const {
        emit(settings, lcd_chars::firmware);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void LcdStream::enableMessages() const {
        emit(settings, enable_system_message_display);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void LcdStream::disableMessages() const {
        emit(settings, disable_system_message_display);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void LcdStream::reset() const {
        emit(settings, lcd_chars::reset);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void LcdStream::color(uint8_t const red, uint8_t const green, uint8_t const blue) const {
        lcd_chars const cmd[] { settings, lcd_chars::color, lcd_chars(red), lcd_chars(green), lcd_chars(blue) };
        write(cmd, sizeof cmd);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void LcdStream::contrast(uint8_t const con) const {
        lcd_chars const cmd[] { settings, lcd_chars::contrast, lcd_chars(con) };
        write(cmd, sizeof cmd);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void LcdStream::redIntensity(uint8_t const intensity) const {
        auto value = map(intensity, 0, 255, 0, 29);
        emit_intensity(uint8_t(backlight_base) + value);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void LcdStream::greenIntensity(uint8_t const intensity) const {
        auto value = map(intensity, 0, 255, 0, 29);
        emit_intensity(uint8_t(backlight_base) + value);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void LcdStream::blueIntensity(uint8_t const intensity) const {
        auto value = map(intensity, 0, 255, 0, 29);
        emit_intensity(uint8_t(backlight_base) + value);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void LcdStream::createChar(uint8_t location, char_map cm) const {
        emit(settings, lcd_chars(27 + location));
        for (auto i { 0 }; i < 8; ++i) {
            write((char)cm[i]);
        }
    }
    //----------------------------------------------------------------------------------------------------------------------
    void LcdStream::writeChar(uint8_t location) const {
        emit(settings, lcd_chars(35 + location));
    }
    //======================================================================================================================
    void LcdStream::emit(lcd_chars const command, lcd_chars const data) const {
        lcd_chars const cmd[] { command, data };
        write(cmd, sizeof cmd);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void LcdStream::emit_intensity(uint8_t const intensity) const {
        emit(settings, lcd_chars(intensity));
    }
    //----------------------------------------------------------------------------------------------------------------------
    void LcdStream::displayChange(bool const on, lcd_chars const disp_bit) const {
        mDisplayState = mDisplayState | disp_bit;
        if ( !on) {
            mDisplayState = mDisplayState & ~disp_bit;
        }
        emit(command, mDisplayState);
    }

} /* namespace lcd */
