#pragma once
//======================================================================================================================
// 2022 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//		 File: LcdStream.h
//
//     Author: rmerriam
//
//    Created: Jan 1, 2022
//
//======================================================================================================================
#include <PiGpio.h>
//----------------------------------------------------------------------------------------------------------------------
namespace lcd {
    enum struct lcd_chars : uint8_t
    ;
    //----------------------------------------------------------------------------------------------------------------------
    class LcdStream {
    public:
        LcdStream(PiGpio const& pigpio);
        virtual ~LcdStream() = 0;

        LcdStream(LcdStream const& other) = delete;
        LcdStream(LcdStream&& other) = delete;
        LcdStream& operator=(LcdStream const& other) = delete;
        LcdStream& operator=(LcdStream&& other) = delete;

        bool operator ()() const;    // tests if device opened
        int16_t error() const {
            return mHandle;
        }

        void reset() const;

        // cursor appearance
        void clear() const;
        void blink(bool const on = true) const;
        void cursor(bool const on = true) const;
        void display(bool const on = true) const;

        // cursor movement
        void home() const;
        void left(uint8_t cnt = 1) const;
        void right(uint8_t cnt = 1) const;
        void move(uint8_t col, uint8_t row) const;
        void scroll(bool const inc_on = true) const;
        void textLtoR(bool const on = true) const;

        // display movement
        void displayLeft(uint8_t cnt = 1) const;
        void displayRight(uint8_t cnt = 1) const;

        // display appearance
        void color(uint8_t const red, uint8_t const green, uint8_t const blue) const;
        void contrast(uint8_t const con = 40) const;
        void firmware() const;
        void redIntensity(uint8_t const intensity) const;
        void greenIntensity(uint8_t const intensity) const;
        void blueIntensity(uint8_t const intensity) const;

        void enableMessages() const;
        void disableMessages() const;

        using char_map = uint8_t[8];
        void createChar(uint8_t location, char_map cm) const;
        void writeChar(uint8_t location) const;

        virtual int16_t write(std::uint8_t const data) const = 0;
        virtual int16_t write(char const* const data) const = 0;
        virtual int16_t write(std::string const& data) const = 0;

    protected:
        PiGpio const& mPiGpio;
        int8_t mHandle { -1 };
        bool mIsOpen {};

        virtual void write(lcd_chars const data[], uint16_t const count) const = 0;

    private:
        mutable lcd_chars mDisplayState;
        mutable lcd_chars mDisplayActions;

        void emit_intensity(uint8_t const intensity) const;
        void displayChange(bool const on, lcd_chars const disp_bit) const;
        void emit(lcd_chars const command, lcd_chars const data) const;
    };
    //----------------------------------------------------------------------------------------------------------------------
    inline bool LcdStream::operator ()() const {
        return mIsOpen;
    }

} /* namespace lcd */

