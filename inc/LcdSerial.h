#pragma once
//======================================================================================================================
// 2022 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//		 File: LcdSerial.h
//
//     Author: rmerriam
//
//    Created: Jan 4, 2022
//
//======================================================================================================================
#include <LcdStream.h>
//----------------------------------------------------------------------------------------------------------------------

namespace lcd {

    class LcdSerial : public LcdStream {
    public:
        LcdSerial(PiGpio const& pigpio, char const* ser_tty = "/dev/serial0", unsigned const baud = 9600);
        ~LcdSerial();

        LcdSerial(LcdSerial const& other) = delete;
        LcdSerial(LcdSerial&& other) = delete;
        LcdSerial& operator=(LcdSerial const& other) = delete;
        LcdSerial& operator=(LcdSerial&& other) = delete;

        virtual int16_t write(std::uint8_t const data) const override;
        virtual int16_t write(char const* const data) const override;
        virtual int16_t write(std::string const& data) const override;

    protected:
        virtual void write(lcd_chars const data[], uint16_t const count) const;
    };

} /* namespace lcd */

