#pragma once
//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//       File: PiGpio.h
//
//     Author: rmerriam
//
//    Created: Jan 1, 2022
//
//======================================================================================================================
#include <stdexcept>

#include <pigpiod_if2.h>
//---------------------------------------------------------------------------------------------------------------------
class PiGpio {
public:
    PiGpio(char const* pi_addr);
    virtual ~PiGpio();

    uint32_t hardware_revision() const;
    uint32_t pigpio_version() const;
    uint32_t if_version() const;
    static int const pi();
    char* error(int16_t err) {
        return pigpio_error(err);
    }

private:
    static inline int16_t mPi;
};
//---------------------------------------------------------------------------------------------------------------------
inline PiGpio::PiGpio(char const* pi_addr) {
    mPi = pigpio_start(const_cast<char*>(pi_addr), 0);

    if (mPi < 0) {
        throw std::runtime_error(std::string("Pigpio could not connect to ") + pi_addr + " with error: " + pigpio_error(mPi));
    }
}
//---------------------------------------------------------------------------------------------------------------------
inline PiGpio::~PiGpio() {
    pigpio_stop(mPi);
}
//---------------------------------------------------------------------------------------------------------------------
inline uint32_t PiGpio::hardware_revision() const {
    return get_hardware_revision(mPi);
}
//---------------------------------------------------------------------------------------------------------------------
inline uint32_t PiGpio::pigpio_version() const {
    return get_pigpio_version(mPi);
}
//---------------------------------------------------------------------------------------------------------------------
inline uint32_t PiGpio::if_version() const {
    return pigpiod_if_version();
}
//---------------------------------------------------------------------------------------------------------------------
inline int const PiGpio::pi() {
    return mPi;
}

