#pragma once
//======================================================================================================================
// 2022 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//		 File: LcdI2c.h
//
//     Author: rmerriam
//
//    Created: Jan 1, 2022
//
//======================================================================================================================
#include <LcdStream.h>
//----------------------------------------------------------------------------------------------------------------------

namespace lcd {

    class LcdI2c : public LcdStream {
    public:
        LcdI2c(PiGpio const& pigpio, uint8_t addr = 0x72, uint8_t dev = 1);
        virtual ~LcdI2c();

        LcdI2c(LcdI2c const& other) = delete;
        LcdI2c(LcdI2c&& other) = delete;
        LcdI2c& operator=(LcdI2c const& other) = delete;
        LcdI2c& operator=(LcdI2c&& other) = delete;

        virtual int16_t write(std::uint8_t const data) const override;
        virtual int16_t write(char const* const data) const override;
        virtual int16_t write(std::string const& data) const override;

    protected:
        virtual void write(lcd_chars const data[], uint16_t const count) const;

    private:
    };

} /* namespace lcd */

